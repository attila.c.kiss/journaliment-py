# Final project

## Web Programming with Python and JavaScript

### Project description:
My final project is a cookbook which I'm going to use in daily basis to help me to loose weight. I looked for a website where I can record my recipes and it gives nutration information about the recipe. There is a lot of website for this but all had something that missed. This course and my new knowledge was a good point to creat a website which fully met my expectations. The project made with **Django, JavaScript and SQLite**.

#### Objectives

- Gain experience with designing and implementing a web application with Python and JavaScript.

#### Requirements

- Your web application must be sufficiently distinct from the other projects in this course, and more complex than those.
- Your web application must utilize at least two of Python, JavaScript, and SQL.
- Your web application must be mobile-responsive.
- In README.md, include a short writeup describing your project, what’s contained in each file you created or modified, and (optionally) any other additional information the staff should know about your project.
- If you’ve added any Python packages that need to be installed in order to run your web application, be sure to add them to requirements.txt!


#### Functions
- The user can **register** for the site.
- The user can **record its weight, height, body fat data and activity level**.
- The site **calculates Body Mass Index and Total Daily Energy Expediture**.
- The site **indicates if the weight data is missing** so it can't calculate personalized data.
- The site **indicates if the weight data is older then a week** and suggest for update.
- The site **shows the weight data history in a chart**.
- The user can set a **target weight and a target calorie intake by the suggestion of the site**.
- The user can **add recipes**.
- The user can **upload a picture** for the recipe.
- The user can **choose commodities** from a list which has been saved before by users.
- The user can **add commodities** if it's not in the list.
- The site **calculates the calorie, protein, fat, carbohydrate, dietry fiber and sodium of the recipe per serving**, using the data of the commodities.
- The site **indicates if the recipe is vegetarian** according the ingredients.
- The site calculates that the recipe's calorie is **how many percentage of the daily planned calorie intake**.
- The site indicates the **energy density** and **sodium level** of the recipe.
- The user can **edit its own recipes**.
- The user can **add/remove** the other users' **recipe to its favorites**.
- The user can **mark steps and ingredients as "done"** by clicking on it.
- The site can work both in **metric** and **imperial unit systems** and the user can **change anytime** between them.
- The admin can manage the units and conversions that available for the ingredients on the Django admin interface.

### Notes:
- I splitted the views.py and forms.py to separate files.

### Files:
- [journaliment]
  - settings.py - Django settings
  - urls.py - Django router
  - wsgi.py - Webserver gateway interface settings
- [media]
  - [images] - recipe images
- [recipes]
  - [forms] - Forms
    - accounts.py
    - commodity.py
    - recipe.py
  - [static]
    - [recipes]
      - [images] - Design images
      - [scripts]
        - common.js - JavaScript code for the recipes
        - profile.js - JavaScript code for the profile calculations
    - [styles]
      - [scss]
        - app.scss - style file
        - general.scss - general styles
        - forms.scss - forms styles
        - profile.scss - styles for the registration and login
        - recipe.scss - styles for the recipes
        - variables.scss - colors
      - app.css - generated css file
      - app.css.map - map for the scss file
  - [templates]
    - [recipes]
      - 404.html - Unique 404 page
      - base.html - Template base
      - commodity-form.html - Template snippet for the commodity-form
      - commodity.html - Template for add/edit commodity (not accessible from menu)
      - edit-recipe.html - Template for edit recipe
      - index.html - Home template
      - menu.html - Menu template
      - recipe-ingredients.html - Template snippet for the ingredients on the recipe editor
      - placeorder.html - Order confirmation page template
      - recipe.html - Template for view recipe
    - [registration]
      - edit.html - Profile editor page template
      - login.html - Login page template
      - password_reset_form.html - New password first page
      - password_reset_done.html - New password second page
      - password_reset_confirm.html - New password third page
      - password_reset_complete.html - New password final page
      - signup.html - Registration form template
  - [views]
    - account.py - Profile logics
    - commodity.py - Commodity logics
    - index.py - Main logics
    - recipe.py - Recipe logics
    - un
  - admin.py - Admin page module registers
  - apps.py - Django apps config
  - forms.py - Custom signup form
  - models.py - Database structure
  - tests.py - Tests
  - urls.py - App router
- db.sqlite3 - Database file
- manage.py - Django manager
- README.md - this file
- requirements.txt - required Python package list for this project
