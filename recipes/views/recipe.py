import json
import decimal

from django.shortcuts import render, redirect
from recipes.forms import RecipeForm, CommodityForm
from django.core import serializers
from django.utils.html import escape
from recipes.views.index import get_conversions
from recipes.models import Profile, Recipe, Unit, Commodity, Ingredient, Step, Conversion

def edit_recipe(request, id):
    request.session.set_expiry(1800)
    units = Unit.objects.all().order_by('type')
    cform = CommodityForm()
    unit_list = []
    ingredients = []
    steps = []

    for unit in units:
        unit_item = {}
        unit_item.update({
            "type": unit.type,
            "system": unit.unit_system,
            "name": unit.name,
            "abbrevation": unit.abbrevation,
            "type": unit.type
        })
        unit_list.append(unit_item)

    if request.method == 'POST':
        form = RecipeForm(request.POST, request.FILES)
        status = 'DRAFT'

        if 'publish' in request.POST:
            status = 'ACTIVE'

        if form.is_valid():
            if id == 0:
                recipe = Recipe()
            else:
                recipe, create = Recipe.objects.get_or_create(pk=id)
            recipe.user = request.user
            recipe.title = form.cleaned_data.get('title')
            if form.cleaned_data.get('image') is not None:
                recipe.image = form.cleaned_data.get('image')
            recipe.intro = form.cleaned_data.get('intro')
            recipe.notes = form.cleaned_data.get('notes')
            recipe.prep_time = form.cleaned_data.get('prep_time')
            recipe.cooking_time = form.cleaned_data.get('cooking_time')
            recipe.serves = form.cleaned_data.get('serves')
            recipe.difficulty = form.cleaned_data.get('difficulty')
            recipe.cals = form.cleaned_data.get('cals')
            recipe.veg = form.cleaned_data.get('veg')
            recipe.protein = form.cleaned_data.get('protein')
            recipe.fat = form.cleaned_data.get('fat')
            recipe.carbs = form.cleaned_data.get('carbs')
            recipe.fiber = form.cleaned_data.get('fiber')
            recipe.sodium = form.cleaned_data.get('sodium')
            recipe.status = status
            recipe.save()
            if form.cleaned_data.get('ingredients'):
                ings = json.loads(form.cleaned_data.get('ingredients'))
                try:
                    Ingredient.objects.filter(recipe=recipe).delete()
                except:
                    print("Nothing to delete.")

                bulk_ings = []
                for ing in ings:
                    save_ing = Ingredient(commodity=Commodity.objects.get(pk=ing['commodity']), recipe=recipe, amount=decimal.Decimal(ing['amount_met']), unit=Unit.objects.get(abbrevation=ing['unit_met']), notes=ing['notes'])
                    bulk_ings.append(save_ing)
                Ingredient.objects.bulk_create(bulk_ings)

            if form.cleaned_data.get('steps'):
                steps = json.loads(form.cleaned_data.get('steps'))
                try:
                    Step.objects.filter(recipe=recipe).delete()
                except:
                    print("Nothing to delete.")

                bulk_steps = []
                for step in steps:
                    if escape(step['step_text']) != "":
                        save_step = Step(recipe=recipe, step_order=int(step['step_order']), step_text=escape(step['step_text']), step_pic=None)
                        bulk_steps.append(save_step)
                Step.objects.bulk_create(bulk_steps)

            return redirect(f'/recipe/{recipe.pk}')
        else:
            print("Something is wrong.")
    else:
        try:
            instance = Recipe.objects.get(pk = id)
            ingredients = get_ingredients(instance)
            steps = get_steps(instance)
            form = RecipeForm(instance=instance)
        except:
            ingredients = None
            steps = None
            form = RecipeForm()

    return render(request, "recipes/edit-recipe.html", {'form': form, 'cform': cform, 'conversions': get_conversions(), 'units': unit_list, 'ingredients': ingredients, 'steps': steps})

def view_recipe(request, id):
    request.session.set_expiry(1800)
    recipe = Recipe.objects.get(pk = id)
    fav = False
    likes = Profile.objects.filter(favorites=recipe).count()
    if recipe:
        if request.user.is_authenticated:
            prof = Profile.objects.get(user = request.user)
            if recipe in prof.favorites.all():
                fav = True
        ingredients = get_ingredients(recipe)
        steps = get_steps(recipe)
        nutritions = calculate_nutrition(request, recipe)
        return render(request, "recipes/recipe.html", {'recipe': recipe, 'ingredients': ingredients, 'steps': steps, 'nutritions': nutritions, 'fav': fav, 'likes': likes})
    else:
        return render(request, "recipes/recipe.html", {'error': 'Recipe error.'})

def add_to_favorites(request, id):
    request.session.set_expiry(1800)
    recipe = Recipe.objects.get(pk = id)
    profile = Profile.objects.get(user = request.user)
    profile.favorites.add(recipe)
    return redirect(f'/recipe/{recipe.pk}')

def remove_from_favorites(request, id):
    request.session.set_expiry(1800)
    recipe = Recipe.objects.get(pk = id)
    profile = Profile.objects.get(user = request.user)
    profile.favorites.remove(recipe)
    return redirect(f'/recipe/{recipe.pk}')

def get_ingredients(recipe):
    ingredients = []
    conversions = {}
    convs = Conversion.objects.all()

    for con in convs:
        conversions[f"{con.from_unit.abbrevation}_{con.to_unit.abbrevation}"] = con.multiplier

    ings = Ingredient.objects.filter(recipe = recipe)

    if ings:
        for ing in ings:
            if ing.unit.abbrevation == 'ml':
                amount_imp = float(ing.amount) / conversions['fl_oz_ml']
                unit_imp = 'fl_oz'
            elif ing.unit.abbrevation == 'g':
                amount_imp = float(ing.amount) / conversions['oz_g']
                unit_imp = 'oz'
            elif ing.unit.abbrevation == 'kg':
                amount_imp = float(ing.amount) / conversions['lb_kg']
                unit_imp = 'lb'
            else:
                amount_imp = ing.amount
                unit_imp = ing.unit.abbrevation

            ing_item = {
                "commodity": ing.commodity.pk,
                "cname": ing.commodity.name,
                "amount_met": round(ing.amount, 2),
                "unit_met":  ing.unit.abbrevation,
                "amount_imp": round(amount_imp, 2),
                "unit_imp": unit_imp,
                "notes": ing.notes
            }
            ingredients.append(ing_item)
    else:
        ingredients = None

    return ingredients

def get_steps(recipe):
    steps = []
    stps = Step.objects.filter(recipe = recipe).order_by('step_order')
    i = 0

    if stps:
        for stp in stps:
            i += 1
            stp_item = {
                "step_order": i,
                "step_text": stp.step_text
            }

            steps.append(stp_item)
    else:
        steps = None

    return steps

def calculate_nutrition(request, recipe):
    ingredients = Ingredient.objects.filter(recipe = recipe)
    serves = recipe.serves
    recipe_calorie = 0
    recipe_protein = 0
    recipe_fat = 0
    recipe_carbs = 0
    recipe_fiber = 0
    full_recipe_sodium = 0
    recipe_weight = 0
    recipe_veg = 'V'
    daily_cal_perc = -1
    energy_density = 0
    sodium_level = 3
    edty = 0
    ed_desc = ''
    sd_desc = ''

    ml = Unit.objects.get(abbrevation = 'ml')
    gram = Unit.objects.get(abbrevation = 'g')

    for ing in ingredients:
        amount_in_metric = 0
        ing_cal = 0
        ing_protein = 0
        ing_fat = 0
        ing_carbs = 0
        ing_fiber = 0
        ing_sodium = 0

        if recipe_veg == 'V' and (ing.commodity.veg == 'O' or ing.commodity.veg == 'L' or ing.commodity.veg == 'OL'):
            recipe_veg = ing.commodity.veg
        elif (recipe_veg == 'O' and ing.commodity.veg == 'L') or (recipe_veg == 'L' and ing.commodity.veg == 'O'):
            recipe_veg = 'OL'
        elif ing.commodity.veg == 'NO':
            recipe_veg = 'NO'

        if ing.unit.unit_system == 'METRIC':
            recipe_weight += ing.amount
            ing_cal = decimal.Decimal(ing.amount) * decimal.Decimal(ing.commodity.cals / 100)
            ing_protein = ing.amount * (ing.commodity.protein / 100)
            ing_fat = ing.amount * (ing.commodity.fat / 100)
            ing_carbs = ing.amount * (ing.commodity.carbs / 100)
            ing_fiber = ing.amount * (ing.commodity.fiber / 100)
            ing_sodium = ing.amount * (ing.commodity.sodium / 100)
        else:
            if ing.unit.type == 'MASS':
                mply = Conversion.objects.get(from_unit = ing.unit, to_unit = gram).multiplier
                amount_in_metric = ing.amount * mply
                recipe_weight += amount_in_metric
                ing_cal = amount_in_metric * (ing.commodity.cals / 100)
                ing_protein = amount_in_metric * (ing.commodity.protein / 100)
                ing_fat = amount_in_metric * (ing.commodity.fat / 100)
                ing_carbs = amount_in_metric * (ing.commodity.carbs / 100)
                ing_fiber = amount_in_metric * (ing.commodity.fiber / 100)
                ing_sodium = amount_in_metric * (ing.commodity.sodium / 100)
            elif ing.unit.type == 'VOLUME':
                mply = Conversion.objects.get(from_unit = ing.unit, to_unit = ml).multiplier
                amount_in_metric = ing.amount * mply
                recipe_weight += amount_in_metric
                ing_cal = ing.amount * mply
                recipe_calorie += amount_in_metric * (ing.commodity.cals / 100)
                ing_protein = amount_in_metric * (ing.commodity.protein / 100)
                ing_fat = amount_in_metric * (ing.commodity.fat / 100)
                ing_carbs = amount_in_metric * (ing.commodity.carbs / 100)
                ing_fiber = amount_in_metric * (ing.commodity.fiber / 100)
                ing_sodium = amount_in_metric * (ing.commodity.sodium / 100)
            else:
                if ing.unit.abbrevation == 'pc(s)':
                    if ing.commodity.g_per_pc:
                        recipe_weight += ing.amount * ing.commodity.g_per_pc
                    ing_cal = ing.amount * ing.commodity.cals
                    ing_protein = ing.amount * ing.commodity.protein
                    ing_fat = ing.amount * ing.commodity.fat
                    ing_carbs = ing.amount * ing.commodity.carbs
                    ing_fiber = ing.amount * ing.commodity.fiber
                    ing_sodium = ing.amount * ing.commodity.sodium
                elif ing.unit.abbrevation == 'to taste':
                    ing_cal = 0
                    ing_protein = 0
                    ing_fat = 0
                    ing_carbs = 0
                    ing_fiber = 0
                    ing_sodium = 0
                elif ing.unit.abbrevation == 'pinch':
                    recipe_weight += decimal.Decimal(ing.amount) * decimal.Decimal(0.4)
                    ing_cal = decimal.Decimal(ing.amount) * decimal.Decimal(0.4) * decimal.Decimal(ing.commodity.cals / 100)
                    ing_protein = ing.amount * decimal.Decimal(0.4) * (ing.commodity.protein / 100)
                    ing_fat = ing.amount * decimal.Decimal(0.4) * (ing.commodity.fat / 100)
                    ing_carbs = ing.amount * decimal.Decimal(0.4) * (ing.commodity.carbs / 100)
                    ing_fiber = ing.amount * decimal.Decimal(0.4) * (ing.commodity.fiber / 100)
                    ing_sodium = ing.amount * decimal.Decimal(0.4) * (ing.commodity.sodium / 100)

        recipe_calorie += ing_cal
        recipe_protein += ing_protein
        recipe_fat += ing_fat
        recipe_carbs += ing_carbs
        recipe_fiber += ing_fiber
        full_recipe_sodium += ing_sodium

    if recipe_weight > 0 and recipe.serves > 0:
        edty = round(recipe_calorie / recipe_weight, 2)

    if edty < 0.6:
        energy_density = 0
        ed_desc = 'Very low energy density'
    elif edty > 0.59 and edty < 1.5:
        energy_density = 1
        ed_desc = 'Low energy density'
    elif edty > 1.49 and edty < 4.01:
        energy_density = 2
        ed_desc = 'Medium energy density'
    else:
        energy_density = 3
        ed_desc = 'High energy density'

    recipe_sodium = full_recipe_sodium / recipe.serves

    if recipe_sodium < 6:
        sodium_level = 0
        sd_desc = 'Sodium free'
    elif recipe_sodium > 5 and recipe_sodium < 36:
        sodium_level = 1
        sd_desc = 'Very low sodium'
    elif recipe_sodium > 35 and recipe_sodium < 141:
        sodium_level = 2
        sd_desc = 'Low sodium'
    elif recipe_sodium > 140 and recipe_sodium < 401:
        sodium_level = 3
        sd_desc = 'Normal sodium level'
    elif recipe_sodium > 400:
        sodium_level = 4
        sd_desc = 'High sodium'

    if request.user.is_authenticated:
        prof = Profile.objects.get(user = request.user)
        if prof.target_calories != None:
            daily_cal_perc = ((recipe_calorie / recipe.serves) / prof.target_calories) * 100

    recipe_nutrition = {
        'cals': int(round(recipe_calorie / recipe.serves, 0)),
        'dcp': daily_cal_perc,
        'veg': recipe_veg,
        'protein': round(recipe_protein / recipe.serves, 2),
        'fat': round(recipe_fat / recipe.serves, 2),
        'carbs': round(recipe_carbs / recipe.serves, 2),
        'fiber': round(recipe_fiber / recipe.serves, 2),
        'sodium': int(round(full_recipe_sodium / recipe.serves, 0)),
        'sodium_level': sodium_level,
        'energy_density': energy_density,
        'sd_desc': sd_desc,
        'ed_desc': ed_desc,
    }

    return recipe_nutrition
