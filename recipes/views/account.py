from django.shortcuts import render, redirect
from django.contrib.auth import login, authenticate
from recipes.forms import SignUpForm, UpdateProfile
from recipes.models import Profile, WeightHistory
from recipes.views.index import get_conversions

def signup(request):
    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            user = form.save()
            user.refresh_from_db()  # load the profile instance created by the signal
            user.profile.email = form.cleaned_data.get('email')
            user.save()
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=user.username, password=raw_password)
            login(request, user)
            return redirect('index')
    else:
        form = SignUpForm()

    return render(request, "registration/signup.html", {'form': form})

def edit_profile(request):
    request.session.set_expiry(1800)
    instance = Profile.objects.get(user = request.user)
    weights = get_weights(request.user)

    if request.method == 'POST':
        form = UpdateProfile(request.POST)
        if form.is_valid():
            profile = Profile.objects.get(user = request.user)
            profile.height = form.cleaned_data.get('height')
            profile.weight = form.cleaned_data.get('weight')
            profile.body_fat = form.cleaned_data.get('body_fat')
            profile.waist = form.cleaned_data.get('waist')
            profile.chest = form.cleaned_data.get('chest')
            profile.hips = form.cleaned_data.get('hips')
            profile.target_weight = form.cleaned_data.get('target_weight')

            profile.target_calories = form.cleaned_data.get('target_calories')
            profile.unit_system = form.cleaned_data.get('unit_system')
            profile.activity = form.cleaned_data.get('activity')
            profile.save()
            wh = WeightHistory(user = request.user, weight = form.cleaned_data.get('weight'), body_fat = form.cleaned_data.get('body_fat'))
            wh.save()
            return redirect('index')
    else:
        form = UpdateProfile(instance=instance)

    return render(request, "registration/edit.html", {'form': form, 'conversions': get_conversions(), 'weights': get_weights(request.user)})

def get_weights(user):
    weights = WeightHistory.objects.filter(user = user).order_by('wdate')
    weight_results = []
    for weight in weights:
        wght = {
            'weight': weight.weight,
            'body_fat': weight.body_fat,
            'date': weight.wdate.strftime("%Y-%m-%d")
            }
        weight_results.append(wght)

    return weight_results
