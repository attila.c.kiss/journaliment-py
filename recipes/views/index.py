from datetime import datetime, timezone
from django.shortcuts import render
from recipes.models import Conversion, Profile, Recipe, WeightHistory

def index(request):
    now = datetime.now(timezone.utc)
    profile_age = -1
    my_recipes = None
    last_20_recipe = None
    favs = None

    if request.user.is_authenticated:
        request.session.set_expiry(1800)
        prof = Profile.objects.get(user = request.user)
        last_updated = WeightHistory.objects.filter(user = request.user).order_by('wdate').reverse()
        if last_updated:
            profile_age = (now - last_updated[0].wdate).days
        my_recipes = Recipe.objects.filter(user = request.user).order_by('-id')
        favs = prof.favorites.all()
        last_20_recipe = Recipe.objects.filter(status='ACTIVE').exclude(user=request.user).order_by('-id')[:20]
    else:
        last_20_recipe = Recipe.objects.filter(status='ACTIVE').order_by('-id')[:20]

    return render(request, "recipes/index.html", {'profile_age': profile_age, 'recipes': last_20_recipe, 'my_recipes': my_recipes, 'favorites': favs})

def get_conversions():
    conversions_obj = Conversion.objects.all()

    if conversions_obj:
        response_data = {}

        for conv in conversions_obj:
            response_data[f"{conv.from_unit.abbrevation}_{conv.to_unit.abbrevation}"] = conv.multiplier

        return response_data
    else:
        return False
