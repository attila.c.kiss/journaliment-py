from django.shortcuts import render, redirect
from django.http import JsonResponse
from recipes.forms import CommodityForm
from recipes.models import Commodity


def commodity(request, id):
    request.session.set_expiry(1800)
    if request.method == 'POST':
        form = CommodityForm(request.POST)
        if form.is_valid():
            commodity = Commodity()
            commodity.name = form.cleaned_data.get('name')
            commodity.cals = form.cleaned_data.get('cals')
            commodity.veg = form.cleaned_data.get('veg')
            commodity.protein = form.cleaned_data.get('protein')
            commodity.fat = form.cleaned_data.get('fat')
            commodity.carbs = form.cleaned_data.get('carbs')
            commodity.fiber = form.cleaned_data.get('fiber')
            commodity.sodium = form.cleaned_data.get('sodium')
            commodity.save()
            return redirect('index')
    else:
        try:
            instance = Commodity.objects.get(pk=id)
            form = CommodityForm(instance=instance)
        except:
            form = CommodityForm()

    return render(request, "recipes/commodity.html", {'cform': form})


def addcommodity(request):
    if request.method == 'POST':
        form = CommodityForm(request.POST)
        if form.is_valid():
            commodity = Commodity()
            commodity.name = form.cleaned_data.get('name')
            commodity.cals = form.cleaned_data.get('cals')
            commodity.veg = form.cleaned_data.get('veg')
            commodity.protein = form.cleaned_data.get('protein')
            commodity.fat = form.cleaned_data.get('fat')
            commodity.carbs = form.cleaned_data.get('carbs')
            commodity.fiber = form.cleaned_data.get('fiber')
            commodity.sodium = form.cleaned_data.get('sodium')
            commodity.save()

            commodity_response = {
                "id": commodity.pk,
                "name": commodity.name,
                "cals": commodity.cals,
                "veg": commodity.veg,
                "protein": commodity.protein,
                "fat": commodity.fat,
                "carbs": commodity.carbs,
                "fiber": commodity.fiber,
                "sodium": commodity.sodium
            }

            return JsonResponse({
                'commodity': commodity_response, 'status': 200
                })
        else:
            return JsonResponse({
                'error': 'Save commodity failed.', 'status': 400
                })


def getcommodities(request):
    if request.method == 'POST':
        search = request.POST.get('filter')
        comms = Commodity.objects.filter(name__icontains=search)
        res = []
        if comms:
            for item in comms:
                itm = {}
                itm.update({
                    "id": item.pk,
                    "name": item.name,
                    "cals": item.cals,
                    "veg": item.veg,
                    "protein": item.protein,
                    "fat": item.fat,
                    "carbs": item.carbs,
                    "fiber": item.fiber,
                    "sodium": item.sodium
                })
                res.append(itm)

        return JsonResponse({'data': res, 'status': 200})
