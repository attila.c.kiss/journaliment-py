document.addEventListener('DOMContentLoaded', () => {
  var com_list = []
  var ingredients = []
  var ingredients_imp = []

  if(localStorage.getItem('unit_system') == null || localStorage.getItem('unit_system') == '') {
    localStorage.setItem('unit_system', profile_usys)
  }

  if(document.querySelector('.navbar-toggler') !== null) {
    document.querySelector('.navbar-toggler').onclick = () => {
      if(document.querySelector('.navbar-collapse').classList.contains('show')) {
        document.querySelector('.navbar-collapse').classList.remove('show')
      } else {
        document.querySelector('.navbar-collapse').classList.add('show')
      }
    }
  }

  change_per_piece = () => {
    if(document.querySelector('#id_per_piece')) {
      if(document.querySelector('#id_per_piece').checked) {
        document.querySelectorAll('.in100').forEach(help => {
          help.style.display = 'none'
        })

        document.querySelectorAll('.in1piece').forEach(help => {
          help.style.display = 'inline'
        })

        if(document.querySelector('.g_per_pc_container')) {
          document.querySelector('.g_per_pc_container').style.display = 'flex'
        }
      } else {
        document.querySelectorAll('.in100').forEach(help => {
          help.style.display = 'inline'
        })

        document.querySelectorAll('.in1piece').forEach(help => {
          help.style.display = 'none'
        })

        if(document.querySelector('.g_per_pc_container')) {
          document.querySelector('.g_per_pc_container').style.display = 'none'
        }
      }
    }
  }

  change_per_piece()

  if(document.querySelector('#id_per_piece')) {
    document.querySelector('#id_per_piece').addEventListener('change', (e) => {
      change_per_piece()
    })
  }

  if(!document.body.classList.contains('edit-profile')) {
    document.body.classList.add(localStorage.getItem('unit_system').toLowerCase())
  }

  if(document.querySelector('#conversion_data')) {
    var conversions = JSON.parse(document.querySelector('#conversion_data').textContent)
  }

  refreshIngredients = () => {
    ing_json = JSON.stringify(ingredients)
    document.querySelector('#id_ingredients').value = ing_json
    if(document.querySelector('#ingredients_list') !== null) {
      document.querySelector('#ingredients_list').innerHTML = ""
      let i = 0
      if(ingredients != null) {
        ingredients.forEach(ing => {
          const ingli = document.createElement('LI')
          const amount = document.createElement('SPAN')
          const amount_imp = document.createElement('SPAN')
          const cname = document.createElement('SPAN')
          const remove_ing = document.createElement('SPAN')
          remove_ing.dataset['idx'] = i
          i += 1
          remove_ing.className = "del-ing"
          remove_ing.append('×')
          remove_ing.onclick = (e) => {
            removeIngredient(e.target.dataset['idx'])
          }

          if(ing.unit_met !== "to taste") {
            amount.append(`${Number.parseFloat(ing.amount_met)} ${ing.unit_met}`)
            cname.append(`${ing.cname}`)
          } else {
            cname.append(`${ing.cname} ${ing.unit_met}`)
          }
          ingli.append(amount, cname)

          if(ing.notes !== "") {
            const sml = document.createElement('small')
            sml.append(`${ing.notes}`)
            ingli.append(sml)
          }

          ingli.append(remove_ing)

          document.querySelector('#ingredients_list').append(ingli)
        })
      }
    }

    if(document.querySelector('#ingredients_list_imp')) {
      document.querySelector('#ingredients_list_imp').innerHTML = ""
      let i = 0
      if(ingredients_imp != null) {
        ingredients_imp.forEach(ing => {
          const ingli = document.createElement('LI')
          const amount = document.createElement('SPAN')
          const amount_imp = document.createElement('SPAN')
          const cname = document.createElement('SPAN')
          const remove_ing = document.createElement('SPAN')
          remove_ing.dataset['idx'] = i
          i += 1
          remove_ing.className = "del-ing"
          remove_ing.append('×')
          remove_ing.onclick = (e) => {
            removeIngredient(e.target.dataset['idx'])
          }

          if(ing.unit !== "to taste") {
            amount.append(`${Number.parseFloat(ing.amount)} ${ing.unit}`)
            cname.append(`${ing.cname}`)
          } else {
            cname.append(`${ing.cname} ${ing.unit}`)
          }
          ingli.append(amount, cname)

          if(ing.notes !== "") {
            const sml = document.createElement('small')
            sml.append(`${ing.notes}`)
            ingli.append(sml)
          }

          ingli.append(remove_ing)

          document.querySelector('#ingredients_list_imp').append(ingli)
        })
      }
    }
  }

  if(localStorage.getItem('unit_system') == 'IMPERIAL' && document.querySelector('#unit-switch') !== null) {
    document.querySelector('#unit-switch').checked = true
  }

  if(document.querySelector('#ingredients_json') != null) {
    ingredients = JSON.parse(document.querySelector('#ingredients_json').textContent)

    ingredients.forEach(ing => {
      let ing_imp = {
        commodity: ing.commodity,
        cname: ing.cname,
        amount: ing.amount_imp,
        unit: ing.unit_imp,
        notes: ing.notes
      }

      ingredients_imp.push(ing_imp)
    })
    console.debug(ingredients_imp)

    refreshIngredients()
  }

  if(document.querySelector('#unit-switch')) {
    document.querySelector('#unit-switch').addEventListener('change', (e) => {
      if(document.querySelector('#unit-switch').checked == true) {
        localStorage.setItem('unit_system', 'IMPERIAL')
      } else {
        localStorage.setItem('unit_system', 'METRIC')
      }
      document.querySelector('body').classList.remove('imperial', 'metric')
      document.querySelector('body').classList.add(localStorage.getItem('unit_system').toLowerCase())

      if(document.querySelector('#units') !== null) {
        document.querySelector('#units').selectedIndex = 0
      }
    })
  }

  if(document.querySelector('#c_search')) {
    document.querySelector('#c_search').onkeyup = (e) => {
      e.preventDefault()
      if(e.code == 'Escape') {
        document.querySelector('#com_search').style.display = 'none'
        document.querySelector('#com_results').style.display = 'none'
        document.querySelector('#c_search').value = ''
      } else {
        const srch = document.querySelector('#c_search').value
        if(srch.length === 3) {
          getCommodities(srch)
        } else if(srch.length < 3) {
          document.querySelector('#com_results').style.display = 'none'
          document.querySelector('#c_search').removeAttribute('data-cid')
        } else {
          document.querySelector('#com_results').style.display = 'block'
          document.querySelector('#c_search').removeAttribute('data-cid')
          let filtered_list = []
          com_list.forEach(item => {
            if(item.name.toLowerCase().indexOf(document.querySelector("#c_search").value.toLowerCase()) > -1) {
              filtered_list.push(item)
            }
          })
          updateComList(filtered_list)
        }
      }
    }
  }

  if(document.querySelector('#add-step')) {
    document.querySelector('#add-step').onclick = function (e) {
      e.preventDefault()
      addStep()
    }
  }

  if(document.querySelector('#unit') !== null) {
    document.querySelector('#unit').addEventListener('change', function() {
      if(this.value === 'to taste') {
        document.querySelector('#amount').disabled = true
      } else {
        document.querySelector('#amount').disabled = false
      }
      this.dataset['sys'] = this.options[this.selectedIndex].className
    })
  }

  if(document.querySelector('#commodity')) {
    document.querySelector('#commodity').onclick = () => {
      document.querySelector('#com_search').style.display = 'block'
      document.querySelector('#c_search').focus()
    }
  }

  if(document.querySelector('.recipe-form') != null) {
    document.querySelector('.recipe-form').onsubmit = () => {
      let steps = []
      let counter = 0
      document.querySelectorAll('.step').forEach(step => {
        let stp = {
          "step_order": counter,
          "step_text": step.value
        }
        steps.push(stp)
        counter++
      })

      document.querySelector('#id_steps').value = JSON.stringify(steps)
    }
  }

  addStep = () => {
    const currentStep = document.querySelectorAll('.step-row').length + 1
    const stepInput = document.createElement('TEXTAREA')
    const stepLabel = document.createElement('LABEL')
    const innerDiv = document.createElement('DIV')
    const outerDiv = document.createElement('DIV')
    stepInput.id = `id_step_${currentStep}`
    stepInput.name = `step-${currentStep}`
    stepInput.classList.add('form-control', 'step')
    stepLabel.classList.add('col-1', 'col-form-label')
    stepLabel.append(`${currentStep}.`)
    innerDiv.classList.add('col-11')
    outerDiv.classList.add('form-group', 'row', 'align-items-start', 'step-row')
    innerDiv.append(stepInput)
    outerDiv.append(stepLabel, innerDiv)
    document.querySelector('#steps_container').append(outerDiv)
  }

  addIngredient = () => {
    let amount = 0
    if(document.querySelector('#unit').value !== 'to taste') {
      if(document.querySelector('#amount').value === '' || document.querySelector('#amount').value === 0) {
        document.querySelector('.amount-alert').innerHTML = 'Set the amount.'
        document.querySelector('.amount-alert').style.display = 'block'
        return false
      } else {
        if(document.querySelector('#unit').selectedIndex === 0) {
          document.querySelector('.amount-alert').innerHTML = 'Set unit.'
          document.querySelector('.amount-alert').style.display = 'block'
          return false
        } else {
          document.querySelector('.amount-alert').style.display = 'none'
          amount = document.querySelector('#amount').value
        }
      }
    } else {
      document.querySelector('.amount-alert').style.display = 'none'
    }

    if(document.querySelector('#commodity').dataset['cid'] === undefined) {
      document.querySelector('.commodity-alert').innerHTML = 'Choose a commodity from the database.'
      document.querySelector('.commodity-alert').style.display = 'block'
      return false
    } else {
      document.querySelector('.commodity-alert').style.display = 'none'
    }

    if(document.querySelector('#unit').dataset['sys'] === 'imp') {
      switch(document.querySelector('#unit').value) {
        case 'lb':
        met_amount = (amount * conversions['lb_kg']).toFixed(2)
        met_unit = 'kg'
        break
        case 'oz':
        met_amount = (amount * conversions['oz_g']).toFixed(2)
        met_unit = 'g'
        break
        case 'fl_oz':
        met_amount = (amount * conversions['fl_oz_ml']).toFixed(2)
        met_unit = 'ml'
        break
        default:
      }
      imp_amount = amount
      imp_unit = document.querySelector('#unit').value
    } else if(document.querySelector('#unit').dataset['sys'] === 'met') {
      switch(document.querySelector('#unit').value) {
        case 'kg':
        imp_amount = (amount / conversions['lb_kg']).toFixed(2)
        imp_unit = 'lb'
        break
        case 'g':
        imp_amount = (amount / conversions['oz_g']).toFixed(2)
        imp_unit = 'oz'
        break
        case 'ml':
        imp_amount = (amount / conversions['fl_oz_ml']).toFixed(2)
        imp_unit = 'fl_oz'
        break
        default:
      }
      met_amount = amount
      met_unit = document.querySelector('#unit').value
    } else {
      imp_amount = amount
      imp_unit = document.querySelector('#unit').value
      met_amount = amount
      met_unit = document.querySelector('#unit').value
    }

    let ing_imp = {
      commodity: document.querySelector('#commodity').dataset['cid'],
      cname: document.querySelector('#commodity').value,
      amount: imp_amount,
      unit: imp_unit,
      notes: document.querySelector('#note').value
    }

    let ing = {
      commodity: document.querySelector('#commodity').dataset['cid'],
      cname: document.querySelector('#commodity').value,
      amount_met: met_amount,
      unit_met: met_unit,
      notes: document.querySelector('#note').value
    }
    ingredients_imp.push(ing_imp)
    ingredients.push(ing)

    document.querySelector('#ingform').reset()
    document.querySelector('#amount').disabled = false
    refreshIngredients()
  }

  removeIngredient = (idx) => {
    ingredients.splice(idx, 1)
    ingredients_imp.splice(idx, 1)
    refreshIngredients()
  }

  if(document.querySelector('#add-ingredient') !== null) {
    document.querySelector('#add-ingredient').onclick = (e) => {
      e.preventDefault()
      addIngredient()
    }
  }

  if(document.querySelector('.modal-header .close') !== null) {
    document.querySelector('.modal-header .close').onclick = (e) => {
      e.preventDefault()
      document.querySelector('.modal').style.display = 'none'
    }
  }

  if(document.querySelector('.modal button[type=submit]') !== null) {
    document.querySelector('.modal button[type=submit]').onclick = (e) => {
      e.preventDefault()
      const request = new XMLHttpRequest()
      request.open('POST', '/addcommodity')

      const header =  "X-CSRFToken"
      request.setRequestHeader(header, csrf_token)

      const data = new FormData(document.querySelector('.commodity-form'))

      request.onload = function() {
        if(this.status >= 200 && this.status < 400) {
          console.debug(JSON.parse(this.response).commodity)
          let resp = JSON.parse(this.response).commodity
          document.querySelector('#commodity').value = resp.name
          document.querySelector('#commodity').dataset['cid'] = resp.id
          document.querySelector('#com_search').style.display = 'none'
          document.querySelector('#com_results').style.display = 'none'
          document.querySelector('.commodity-form').reset()
          document.querySelector('.modal').style.display = 'none'
        }
      }
      request.onerror = function() {}

      request.send(data)
    }
  }

  updateComList = (list) => {
    document.querySelector('#com_results').innerHTML = ""
    if(list.length > 0) {
      list.forEach(commodity => {
        const com_item = document.createElement('LI')
        com_item.append(commodity.name)
        com_item.onclick = function() {
          document.querySelector('#commodity').value = commodity.name
          document.querySelector('#commodity').dataset['cid'] = commodity.id
          document.querySelector('#com_search').style.display = 'none'
          document.querySelector('#com_results').style.display = 'none'
        }

        document.querySelector('#com_results').append(com_item)
        document.querySelector('#com_results').style.display = 'block'
        document.querySelector('#com_search').style.display = 'block'
      })
    } else {
      const nores_cont = document.createElement('div')
      nores_cont.classList.add('container')
      const nores = document.createElement('div')
      nores.classList.add('row', 'no-res', 'justify-content-between', 'align-items-center')
      nores_text = document.createElement('SPAN')
      nores_text.append("No results.")
      nores.append(nores_text)
      let add_btn = document.createElement('BUTTON')
      add_btn.classList.add('btn', 'btn-primary')
      add_btn.append('Add new')
      add_btn.onclick = function(e) {
        e.preventDefault()
        document.querySelector('.commodity-form').reset()
        document.querySelector('.g_per_pc_container').style.display = 'none'
        document.querySelector('.modal').style.display = 'block'
        document.querySelector('#id_name').focus()
      }
      nores.append(add_btn)
      nores_cont.append(nores)
      document.querySelector('#com_results').append(nores_cont)
      document.querySelector('#com_results').style.display = 'block'
      document.querySelector('#com_search').style.display = 'block'
    }
  }

  getCommodities = (filter) => {
    const request = new XMLHttpRequest()
    request.open('POST', '/getcommodities')

    const header =  "X-CSRFToken"
    request.setRequestHeader(header, csrf_token)

    const data = new FormData()
    data.append('filter', filter)

    request.onload = function() {
      if(this.status >= 200 && this.status < 400) {
        com_list = JSON.parse(this.response).data
        document.querySelector('#com_results').innerHTML = ""
        updateComList(com_list)
        return com_list
      }
    }
    request.onerror = function() {}

    request.send(data)
  }

  energyDensity = (kcal) => {
    if(Number.parseInt(kcal)) {
      let ed = kcal / 100
      let ed_desc = ''

      if(ed < 0.6) {
        ed = 'very low'
      } else if(ed > 0.59 && ed < 1.5) {
        ed = 'low'
      } else if(ed > 1.49 && ed < 4) {
        ed = 'medium'
      } else {
        ed = 'high'
      }
      return ed

    } else {
      return 'N/A'
    }
  }

  if (document.querySelector('.cloak') != null) {
    document.querySelector('.cloak').style.display = 'block'
    document.querySelector('.cloak').style.opacity = '1'
  }
})
