document.addEventListener('DOMContentLoaded', () => {
  if(document.querySelector('#weights_data')) {
    var weight_history = JSON.parse(document.querySelector('#weights_data').textContent)
  }

  if(document.querySelector('#conversion_data') !== null) {
    var conversions = JSON.parse(document.querySelector('#conversion_data').textContent)
  }

  document.querySelectorAll("[id$='_imp']").forEach(inp_imp => {
    const metric = document.querySelector(`#${inp_imp.id.substr(0, inp_imp.id.search('_imp'))}`)
    const imperial = inp_imp
    let m_prec, i_prec = 2

    if(metric.dataset['precision']) {
      m_prec = metric.dataset['precision']
    }

    if(imperial.dataset['precision']) {
      i_prec = imperial.dataset['precision']
    }

    imperial.value = (metric.value / conversions[`${imperial.dataset['unit']}_${metric.dataset['unit']}`]).toFixed(imperial.dataset['precision'])

    imperial.addEventListener('change', e => {
      const met = document.querySelector(`#${e.target.id.substr(0, inp_imp.id.search('_imp'))}`)
      const imp = document.querySelector(`#${e.target.id}`)
      met.value = (imp.value * conversions[`${imp.dataset['unit']}_${met.dataset['unit']}`]).toFixed(met.dataset['precision'])
    })

    metric.addEventListener('change', e => {
      const met = document.querySelector(`#${e.target.id}`)
      const imp = document.querySelector(`#${e.target.id}_imp`)
      imp.value = (met.value / conversions[`${imp.dataset['unit']}_${met.dataset['unit']}`]).toFixed(imp.dataset['precision'])
    })
  })

  document.querySelectorAll('input[name=unit_system]').forEach(radioButton => {
    radioButton.addEventListener('change', e => {
      if(document.querySelector('input[name=unit_system]:checked').value == 'METRIC') {
        document.querySelector('body').classList.remove('imperial')
        document.querySelector('body').classList.add('metric')
        if(document.querySelector('#s_weight').value != '') {
          drawChart('METRIC', document.querySelector('#s_weight').value)
        }
      } else {
        document.querySelector('body').classList.remove('metric')
        document.querySelector('body').classList.add('imperial')
        if(document.querySelector('#s_weight').value != '') {
          drawChart('IMPERIAL', document.querySelector('#s_weight').value)
        }
      }
    })
  })

  var calculateBMI = (height, weight) => {
    return (weight/((height/100)**2)).toFixed(1)
  }

  var reverseBMI = (height, bmi) => {
    return (bmi*(height/100)**2).toFixed(2)
  }

  var calculateBMR = (weight, body_fat) => {
    return 370+(21.6*(weight*(1-body_fat/100)))
  }

  var profileBMI = () => {
    if(document.querySelector('#id_height').value != "" && document.querySelector('#id_weight').value != "") {
      const bmi = calculateBMI(document.querySelector("#id_height").value, document.querySelector("#id_weight").value)
      document.querySelector('.bmi').innerHTML = bmi

      const bmi_desc = document.createElement('span')

      if(bmi < 16.1) {
        document.querySelector('.bmi').className = 'bmi severe'
        bmi_desc.append('Severe Thinness')
      } else if(bmi > 16 && bmi < 17.1) {
        document.querySelector('.bmi').className = 'bmi moderate'
        bmi_desc.append('Moderate Thinness')
      } else if(bmi > 17 && bmi < 18.6) {
        document.querySelector('.bmi').className = 'bmi mild'
        bmi_desc.append('Mild Thinness')
      } else if(bmi > 18.5 && bmi < 25.1) {
        document.querySelector('.bmi').className = 'bmi normal'
        bmi_desc.append('Normal')
      } else if(bmi > 25 && bmi < 30.1) {
        document.querySelector('.bmi').className = 'bmi overweight'
        bmi_desc.append('Overweight')
      } else if(bmi > 30 && bmi < 35.1) {
        document.querySelector('.bmi').className = 'bmi obese1'
        bmi_desc.append('Obese Class I')
      } else if(bmi > 35 && bmi < 40.1) {
        document.querySelector('.bmi').className = 'bmi obese2'
        bmi_desc.append('Obese Class II')
      } else {
        document.querySelector('.bmi').className = 'bmi obese3'
        bmi_desc.append('Obese Class III')
      }

      document.querySelector('.bmi').append(bmi_desc)

      const sweight_from = reverseBMI(document.querySelector("#id_height").value, 18.6)
      const sweight_to = reverseBMI(document.querySelector("#id_height").value, 25)
      const sweight_from_imp = (reverseBMI(document.querySelector("#id_height").value, 18.6) / conversions['lb_kg']).toFixed(2)
      const sweight_to_imp = (reverseBMI(document.querySelector("#id_height").value, 25) / conversions['lb_kg']).toFixed(2)

      document.querySelector('#s_weight').value = sweight_to
      document.querySelector('.suggested_weight').innerHTML = `Suggested target weight based on your normal BMI is between ${sweight_from} kg and ${sweight_to} kg.`
      document.querySelector('.suggested_weight_imp').innerHTML = `Suggested target weight based on your normal BMI is between ${sweight_from_imp} lbs and ${sweight_to_imp} lbs.`

      drawChart(localStorage.getItem('unit_system'), sweight_to)
    } else {
      document.querySelector('.bmi').className = 'bmi'
      document.querySelector('.bmi').innerHTML = "N/A"
      document.querySelector('.suggested_weight').innerHTML = ""
      document.querySelector('.suggested_weight_imp').innerHTML = ""
    }

    if(document.querySelector('#id_height').value != "" && document.querySelector('#id_weight').value != "") {
      let mly = 1

      switch (document.querySelector('select[name=activity]').value) {
        case 'SEDENTARY':
        mly = 1.2
        break
        case 'LIGHTLY_ACTIVE':
        mly = 1.375
        break
        case 'MODERATELY_ACTIVE':
        mly = 1.55
        break
        case 'VERY_ACTIVE':
        mly = 1.725
        break
        case 'EXTRA_ACTIVE':
        mly = 1.9
        break
        default:
        mly = 1
      }


      if(document.querySelector('#id_body_fat').value != "") {
        const tdee = (calculateBMR(document.querySelector('#id_weight').value, document.querySelector('#id_body_fat').value) * mly).toFixed(0)
        document.querySelector('.tdee').innerHTML = `${tdee}<span>kcal/day</span>`
        const caldef = tdee - 1000
        document.querySelector('.target_calories_help').innerHTML = `The recommended minimum target calories for your effective weight loss is <span class="minkcal" title="Click to set">${caldef} kcal/day</span>`
        document.querySelector('.minkcal').onclick = (e) => {
          document.querySelector("#id_target_calories").value = Number.parseInt(e.target.innerHTML)
        }
      }
    }
  }

  var drawChart = (unit_system, target_weight) => {

    console.debug(unit_system)
    let ctx = document.getElementById('myChart').getContext('2d')

    let weight_chart = []
    let labels = []
    let normal_weight = []
    let weight_unit = 'kg'

    if(unit_system == 'IMPERIAL') {
      weight_unit = 'lbs'
    }

    weight_history.forEach(weight_data => {
      labels.push(weight_data.date)
      if(unit_system == 'METRIC') {
        weight_chart.push(Number.parseFloat(weight_data.weight))
        normal_weight.push(Number.parseFloat(target_weight))
      } else {
        weight_chart.push(Number.parseFloat(weight_data.weight / conversions['lb_kg']).toFixed(2))
        normal_weight.push(Number.parseFloat(target_weight / conversions['lb_kg']).toFixed(2))
      }
    })

    let myChart = new Chart(ctx, {
      type: 'line',
      data: {
        labels: labels,
        datasets: [{
          label: `Weight (${weight_unit})`,
          data: weight_chart,
        },{
          label: 'Target',
          data: normal_weight,
          borderColor: '#faa',
        }]
      },
      options: {
        scales: {
          xAxes: [{
            type: 'time',
            time: {
              unit: 'day',
              displayFormats: {
                day: 'DD.MM.\'YY'
              }
            }
          }],
          yAxes: [{
            ticks: {
              suggestedMin: Number.parseInt(normal_weight[0] - 2),
            }
          }]
        }
      }
    })
  }

  profileBMI()

  document.querySelector('#id_height').addEventListener('change', profileBMI)
  document.querySelector('#id_weight').addEventListener('change', profileBMI)
  document.querySelector('#id_activity').addEventListener('change', profileBMI)
  document.querySelector('#id_body_fat').addEventListener('change', profileBMI)
})
