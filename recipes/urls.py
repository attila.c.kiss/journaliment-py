from django.urls import path, re_path

from . import views


urlpatterns = [
    path("", views.index, name="index"),
    path("signup", views.signup, name="signup"),
    path("edit", views.edit_profile, name="edit_profile"),

    path("commodity/<int:id>", views.commodity, name="commodity"),
    path("getcommodities", views.getcommodities),
    path("addcommodity", views.addcommodity),

    path("recipe/<int:id>", views.view_recipe, name="view_recipe"),
    path("edit_recipe/<int:id>", views.edit_recipe, name="edit_recipe"),
    path("add_to_favorites/<int:id>", views.add_to_favorites, name="add_to_favorites"),
    path("remove_from_favorites/<int:id>", views.remove_from_favorites, name="remove_from_favorites"),
]
