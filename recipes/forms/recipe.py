from django import forms
from recipes.models import Recipe

class RecipeForm(forms.ModelForm):
    ingredients = forms.CharField(required=False)
    steps = forms.CharField(required=False)

    class Meta:
        model = Recipe
        fields = ('title', 'image', 'intro', 'notes', 'prep_time', 'cooking_time', 'serves', 'difficulty')

        def __init__(self, *args, **kwargs):
            super(Commodity, self).__init__(*args, **kwargs)
            self.fields['intro'].required = False
            self.fields['notes'].required = False
            self.fields['protein'].required = False
            self.fields['fat'].required = False
            self.fields['carbs'].required = False
            self.fields['fiber'].required = False
            self.fields['sodium'].required = False
            self.fields['cals'].required = False
            self.fields['veg'].required = False
            self.fields['image'].required = False
