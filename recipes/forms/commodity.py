from django import forms
from recipes.models import Commodity

class CommodityForm(forms.ModelForm):
    VEG_LEVELS = [('NO', 'No'),
                    ('OL', 'Ovo-lacto vegetarian'),
                    ('O',  'Ovo vegetarian'),
                    ('L',  'Lacto vegetarian'),
                    ('V',  'Vegan')]
    veg = forms.ChoiceField(choices=VEG_LEVELS)

    class Meta:
        model = Commodity
        fields = ('name', 'per_piece', 'cals', 'veg', 'protein', 'fat', 'carbs', 'fiber', 'sodium')

        def __init__(self, *args, **kwargs):
            super(Commodity, self).__init__(*args, **kwargs)
            self.fields['veg'].default = None
            self.fields['protein'].required = False
            self.fields['fat'].required = False
            self.fields['carbs'].required = False
            self.fields['fiber'].required = False
            self.fields['sodium'].required = False
