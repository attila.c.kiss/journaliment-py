from django import forms
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
from recipes.models import Profile

class SignUpForm(UserCreationForm):

    email = forms.EmailField(max_length=64)

    class Meta:
        model = User
        fields = ('username', 'email', 'password1', 'password2')

    def __init__(self, *args, **kwargs):
        super(SignUpForm, self).__init__(*args, **kwargs)
        self.fields['username'].widget = forms.TextInput(attrs={'class': 'form-control'})
        self.fields['email'].widget = forms.EmailInput(attrs={'class': 'form-control'})
        self.fields['password1'].widget = forms.PasswordInput(attrs={'class': 'form-control'})
        self.fields['password2'].widget = forms.PasswordInput(attrs={'class': 'form-control'})

class UpdateProfile(forms.ModelForm):

    UNIT_SYSTEMS = [('IMPERIAL', 'imperial'),
                    ('METRIC', 'metric')]
    unit_system = forms.ChoiceField(choices=UNIT_SYSTEMS, widget=forms.RadioSelect)

    class Media:
        js = ('recipes/scripts/profile.js',)

    class Meta:
        model = Profile
        fields = ('unit_system', 'height', 'weight', 'body_fat', 'target_weight', 'activity', 'target_calories')

    def __init__(self, *args, **kwargs):
        super(UpdateProfile, self).__init__(*args, **kwargs)
        self.fields['height'].required = False
        self.fields['weight'].required = False
        self.fields['body_fat'].required = False
        self.fields['target_weight'].required = False
        self.fields['activity'].required = False
        self.fields['target_calories'].required = False
