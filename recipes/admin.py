from django.contrib import admin
from .models import Commodity, Profile, Unit, Conversion, Recipe, Step, Ingredient, WeightHistory

class CommodityAdmin(admin.ModelAdmin):
    list_display = ('name', 'cals', 'veg', 'protein', 'fat', 'carbs', 'fiber', 'sodium', 'verified')

class ConversionAdmin(admin.ModelAdmin):
    list_display = ('from_unit', 'to_unit', 'multiplier')

class WeightHistoryAdmin(admin.ModelAdmin):
    list_display = ('user', 'weight', 'body_fat', 'wdate')

class RecipeAdmin(admin.ModelAdmin):
    list_display = ('user', 'title', 'created', 'last_mod')

admin.site.register(Commodity, CommodityAdmin)
admin.site.register(Profile)
admin.site.register(WeightHistory, WeightHistoryAdmin)
admin.site.register(Recipe, RecipeAdmin)
admin.site.register(Unit)
admin.site.register(Ingredient)
admin.site.register(Step)
admin.site.register(Conversion, ConversionAdmin)
