# Generated by Django 3.0.6 on 2020-06-18 13:29

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('recipes', '0022_commodity_g_per_pc'),
    ]

    operations = [
        migrations.AlterField(
            model_name='commodity',
            name='unit_type',
            field=models.CharField(choices=[('LENGTH', 'length'), ('MASS', 'mass'), ('VOLUME', 'volume'), ('OTHER', 'other')], default='MASS', max_length=6),
        ),
        migrations.CreateModel(
            name='Cookbook',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('recipe', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='c_recipe', to='recipes.Recipe')),
                ('user', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
            ],
        ),
    ]
