# Generated by Django 3.0.6 on 2020-06-06 10:25

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('recipes', '0007_remove_profile_sex'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='profile',
            name='year_of_birth',
        ),
    ]
