from django.db import models
from django.contrib.auth.models import User
from django.db.models.signals import post_save
from django.dispatch import receiver

UNIT_SYSTEMS = (
    ('IMPERIAL', 'imperial'),
    ('METRIC',   'metric'),
    ('GLOBAL',   'global')
)

UNIT_TYPES = (
    ('LENGTH', 'length'),
    ('MASS',   'mass'),
    ('VOLUME', 'volume'),
    ('OTHER',  'other')
)

ACTIVITY_LEVELS = (
    ('SEDENTARY',         'Little or no exercise, desk job'),
    ('LIGHTLY_ACTIVE',    'Light exercise/sports 1-3 days/week'),
    ('MODERATELY_ACTIVE', 'Moderate exercise/sports 6-7 days'),
    ('VERY_ACTIVE',       'Hard exercise every day, or 2 xs/day'),
    ('EXTRA_ACTIVE',      'Hard exercise 2 or more times per day'),
)

VEG_LEVELS = (
    ('NO', 'No'),
    ('OL', 'Ovo-lacto vegetarian'),
    ('O',  'Ovo vegetarian'),
    ('L',  'Lacto vegetarian'),
    ('V',  'Vegan'),
)

DIFFICULTY_LEVELS = (
    ('1',      'Easy'),
    ('2',   'Average'),
    ('3', 'Difficult'),
)

STATUSES = (
    ('ACTIVE', 'active'),
    ('DRAFT', 'draft'),
    ('REPORTED', 'reported')
)

class WeightHistory(models.Model):
    user            = models.ForeignKey(User, on_delete=models.SET_NULL, related_name='weight_user', null=True)
    weight          = models.DecimalField(max_digits=5, decimal_places=2)
    body_fat        = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True)
    wdate           = models.DateTimeField(auto_now_add=True)

class Unit(models.Model):
    name            = models.CharField(max_length=30)
    abbrevation     = models.CharField(max_length=10)
    type            = models.CharField(max_length=6, default='OTHER', choices=UNIT_TYPES)
    unit_system     = models.CharField(max_length=8, choices=UNIT_SYSTEMS, default='METRIC')

    def __str__(self):
        return self.name

class Conversion(models.Model):
    from_unit       = models.ForeignKey(Unit, on_delete=models.CASCADE, related_name='from_unit')
    to_unit         = models.ForeignKey(Unit, on_delete=models.CASCADE, related_name='to_unit')
    multiplier      = models.FloatField(default=1)

class Commodity(models.Model):
    name            = models.CharField(max_length=127, default='', unique=True)
    per_piece       = models.BooleanField(default=False)
    cals            = models.PositiveIntegerField(null=True, blank=True, default=0, verbose_name="Calories")
    veg             = models.CharField(max_length=2, choices=VEG_LEVELS, null=True, blank=True, default=None, verbose_name="Vegetarian")
    protein         = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True, verbose_name="Protein")
    fat             = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True, verbose_name="Fat")
    carbs           = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True, verbose_name="Carbs")
    fiber           = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True, verbose_name="Fiber")
    sodium          = models.DecimalField(max_digits=7, decimal_places=2, null=True, blank=True, verbose_name="Sodium")
    verified        = models.IntegerField(default=0)
    unit_type       = models.CharField(max_length=6, default='MASS', choices=UNIT_TYPES)
    g_per_pc        = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True)

    def __str__(self):
        return self.name

class Recipe(models.Model):
    user            = models.ForeignKey(User, on_delete=models.SET_NULL, related_name='author', null=True)
    created         = models.DateTimeField(auto_now_add=True)
    last_mod        = models.DateTimeField(auto_now=True)
    title           = models.CharField(max_length=255, default='')
    intro           = models.TextField(max_length=500, verbose_name='Introduction')
    notes           = models.TextField(max_length=2000, null=True, blank=True)
    prep_time       = models.PositiveIntegerField(default=0)
    cooking_time    = models.PositiveIntegerField(default=0)
    serves          = models.PositiveIntegerField(null=True)
    difficulty      = models.CharField(max_length=9, choices=DIFFICULTY_LEVELS, default='2')
    likes           = models.IntegerField(default=0)
    times           = models.PositiveIntegerField(default=0)
    cals            = models.PositiveIntegerField(null=True)
    protein         = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True, verbose_name="Protein (g/100g)")
    fat             = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True, verbose_name="Fat (g/100g)")
    carbs           = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True, verbose_name="Carbohydrate (g/100g)")
    fiber           = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True, verbose_name="Dietry Fiber (g/100g)")
    sodium          = models.DecimalField(max_digits=7, decimal_places=2, null=True, blank=True, verbose_name="Sodium (mg/100g)")
    veg             = models.CharField(max_length=2, choices=VEG_LEVELS, null=True, blank=True, default=None, verbose_name="Vegetarian")
    image           = models.ImageField(upload_to='images/', null=True, blank=True)
    status          = models.CharField(max_length=12, choices=STATUSES, default='DRAFT')

class Step(models.Model):
    recipe          = models.ForeignKey(Recipe, on_delete=models.CASCADE, related_name='s_recipe')
    step_order      = models.PositiveIntegerField(default=0)
    step_text       = models.TextField(max_length=1000)
    step_pic        = models.TextField(max_length=255, null=True, blank=True, default=None)

class Ingredient(models.Model):
    commodity       = models.ForeignKey(Commodity, on_delete=models.CASCADE, related_name='commodity')
    recipe          = models.ForeignKey(Recipe, on_delete=models.CASCADE, related_name='i_recipe')
    group           = models.PositiveSmallIntegerField(default=0)
    amount          = models.DecimalField(max_digits=5, decimal_places=2, null=True, default=0)
    unit            = models.ForeignKey(Unit, on_delete=models.CASCADE, related_name='unit')
    notes           = models.CharField(max_length=64, blank=True)

class Profile(models.Model):
    user            = models.OneToOneField(User, on_delete=models.CASCADE, related_name='profile')
    email           = models.EmailField(verbose_name="Email Address", unique=True)
    height          = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True)
    weight          = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True)
    body_fat        = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True)
    target_weight   = models.DecimalField(max_digits=5, decimal_places=2, null=True, blank=True)
    target_calories = models.IntegerField(null=True, blank=True)
    last_updated    = models.DateTimeField(auto_now=True, blank=True)
    unit_system     = models.CharField(max_length=8, choices=UNIT_SYSTEMS, default='METRIC')
    activity        = models.CharField(max_length=17, choices=ACTIVITY_LEVELS, default='LIGHTLY_ACTIVE')
    favorites       = models.ManyToManyField(Recipe, blank=True, related_name="favroites")

@receiver(post_save, sender=User)
def update_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)
    instance.profile.save()
